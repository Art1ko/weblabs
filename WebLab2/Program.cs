﻿using System;

namespace WebLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.StackEvent += StackReached;
            stack.Push(2, 3);
            var p = stack.Pop();
            var b = stack.Pop();
            var a = stack.Pop();
            
            stack.Push(1, 2, 3, 4, 6);
            Console.WriteLine(stack.Peek());
            Console.WriteLine($"Count: { stack.Count}" );

            Stack<Person> pplStack = new Stack<Person>();
            pplStack.Push(new Person { Name = "Anton" }, 
                new Person { Name = "Aa" });
            foreach (var value in pplStack)
                Console.WriteLine(value.Name);
            Console.WriteLine(stack.Contains(4));
            Console.ReadLine();
        }

        private static void StackReached<T>(object sender, StackEventReached<T> e)
        {
            Console.WriteLine($"Value {e.data} was {e.Message} at {e.DateTime}");

        }
    }
}
