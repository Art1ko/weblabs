﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WebLab2
{
    public class LinkedList<T> : IEnumerable<T>, ICollection<T>
    {
        Node<T> Top;
        public int Count { get; private set; }
        public bool IsEmpty { get { return Count == 0; } }

        public bool IsReadOnly => throw new NotImplementedException();

        public LinkedList()
        {
            this.Top = null;
        }

        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (Top == null)
            {
                node.Next = null;
            }
            else
            {
                node.Next = Top;
            }
            Top = node;
            Count++;
        }

        public T GetTopElement()
        {
            try
            {

                if (Top == null)
                    throw new InvalidOperationException("The stack is empty");
                else
                {
                    return Top.Data;
                }
            }
            catch(InvalidOperationException)
            {
                return default(T);
            }
        }

        public T Remove() 
        {
            try
            {
                if (Top == null)
                    throw new InvalidOperationException("The stack is empty");
                else
                {
                    var value = Top.Data;
                    Top = Top.Next;
                    Count--;
                    return value;
                }
            }
            catch(InvalidOperationException)
            {
                return default(T);
            }
        }

        public bool Remove(T data)
        {
            Node<T> current = Top;
            Node<T> previous = null;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                    }
                    else
                    {
                        Top = Top.Next;
                    }
                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }
            return false;
        }

        public void Clear()
        {
            Top = null;
            Count = 0;
        }

        public bool Contains(T data)
        {
            Node<T> current = Top;
            while (current != null)
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            return false;
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = Top;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        
    }
}