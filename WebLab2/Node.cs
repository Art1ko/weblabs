﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebLab2
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
            Next = null;
        }

        public T Data { get; set; }

        public Node<T> Next { get; set; }
    }
}
