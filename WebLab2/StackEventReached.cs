﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebLab2
{
    public class StackEventReached<T>: EventArgs
    {
        public string Message;
        public T data;
        public DateTime DateTime;
    }
}
