﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WebLab2
{
    public class Stack<T> : IEnumerable<T>
    {
        private LinkedList<T> _linkedList;

        public event EventHandler<StackEventReached<T>> StackEvent;


        public int Count { get { return _linkedList.Count ; } }
        public bool IsEmpty { get { return _linkedList.IsEmpty; } }

        public Stack()
        {
            _linkedList = new LinkedList<T>();  
        }

        private StackEventReached<T> CreateEvent(string msg, T Data)
        {
            StackEventReached<T> args = new StackEventReached<T>()
            {
                Message = msg,
                data = Data,
                DateTime = DateTime.Now
            };
            return args;
        }

        public void Push(params T[] values)
        {
            foreach (var i in values)
            {
                _linkedList.Add(i);
               
                OnStackReached(CreateEvent("added", i));
            }
        }

        protected void OnStackReached(StackEventReached<T> e)
        {
            StackEvent?.Invoke(this, e);
        }

        public bool Contains(T data)
        {
            return _linkedList.Contains(data);
        }


        public void Clear()
        {
            _linkedList.Clear();
        }

        public T Pop()
        {
           
            var value = _linkedList.Remove();
            OnStackReached(CreateEvent("popped", value));
            return value;
        }

        public T Peek()
        {
            var value = _linkedList.GetTopElement();
            OnStackReached(CreateEvent("peeked",value));
            return _linkedList.GetTopElement();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_linkedList).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)_linkedList).GetEnumerator();
        }
    }
}
